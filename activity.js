db.fruits.insertMany([
    {
        "name": "Banana",
        "supplier": "Farmer Fruits Inc.",
        "stocks": 30,
        "price": 20,
        "onSale": true
    },
    {
        "name": "Mango",
        "supplier": "Mango Magic Inc.",
        "stocks": 50,
        "price": 70,
        "onSale": true
    },
    {
        "name": "Dragon Fruit",
        "supplier": "Farmer Fruits Inc.",
        "stocks": 10,
        "price": 60,
        "onSale": true
    },
    {
        "name": "Grapes",
        "supplier": "Fruity Co.",
        "stocks": 30,
        "price": 100,
        "onSale": true
    },
    {
        "name": "Apple",
        "supplier": "Apple Valley",
        "stocks": 0,
        "price": 20,
        "onSale": false
    },
    {
        "name": "Papaya",
        "supplier": "Fruity Co.",
        "stocks": 15,
        "price": 60,
        "onSale": true
    }
]);

/*  
    PROBLEM 1
    - Use the count operator to count the total number of fruits on sale.
*/

    db.fruits.aggregate ([
            //stage1 - filter out all onSale fruits
            {
                $match: 
                {   "onSale" : true
                }
            },
            //stage2 - count all total number of fruits on sale
            {
                $count : "totalNumberOfFruitsOnSale"
            }
        ]);

/*
    PROBLEM 2
    - Use the count operator to count the total number of fruits with stock more than 20.
*/
    db.fruits.aggregate ([
            //stage 1 - filter out all fruits w/ stock gt 20
            {
                $match : {"stocks" :{$gt : 20} }
            },
            //stage2 - count all fruit with stocks more than 20
            {
                $count : "fruitsWithStocksMoreThan20"
            }
        ]);

/*
    PROBLEM 3 
    - Use the average operator to get the average price of fruits on Sale per supplier.
*/
    db.fruits.aggregate ([
            //stage1 - filter out all onSale fruits
            {
                $match: 
                {   "onSale" : true
                }
            },
            //stage2 - group accdg to supplier, then get average
            {
                $group :
                    {   _id : "$supplier" , average: {$avg: "$price"}
                    }
            }
        ]);

/*
    PROBLEM 4
    - Use the max operator to get the highest price of a fruit per supplier.
*/
    db.fruits.aggregate ([
            //stage1 - filter out all onSale fruits
            {
                $match: 
                {   "onSale" : true
                }
            },
            //stage2 - get the highest price of fruit per supplier
            {
                $group:
                    {   _id : "$supplier" , highestPrice : 
                        {$max : "$price"} 
                    }
            }
        ]);

/*
    PROBLEM 5
    - Use the min operator to get the lowest price of a fruit per supplier.
*/
    db.fruits.aggregate ([
            //stage1 - filter out all onSale fruits
            {
                $match: 
                {   "onSale" : true
                }
            },
            //stage2 - get the highest price of fruit per supplier
            {
                $group:
                    {   _id : "$supplier" , lowestPrice : 
                        {$min : "$price"} 
                    }
            }
        ]);
